package chart;

public class Point {
	
	private double x;
	private double y;
	
	
	public Point(int i, int j) {
		x = i;
		y = j;
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	

}
